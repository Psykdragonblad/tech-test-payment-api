﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Enum;
using Tech_test_paymant_api.Validators;

namespace Tech_test_paymant_api.Models
{
    /// <summary>
    /// Classe de Vendas
    /// </summary>
    public class Venda
    {
        /// <summary>
        /// Id da venda
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Data de criação da venda
        /// </summary>
        public DateTime DataVenda { get; set; }

        /// <summary>
        /// Data de alteração
        /// </summary>
        public DateTime DataAlteracao { get; set; }

        /// <summary>
        /// Status da venda
        /// </summary>
        [EnumDataType(typeof(StatusVenda))]
        public StatusVenda StatusVenda { get; set; }

        /// <summary>
        /// Produtos da venda
        /// </summary>
        public List<Produto> Produtos { get; set; }

        /// <summary>
        /// Id de vendedor
        /// </summary>
        public int IdVendedor { get; set; }

        /// <summary>
        /// Método Utilizado para validar se existe produto e vendedor
        /// </summary>
        /// <returns></returns>
        public bool EhValido()
        {
            return VendaValidacao.ExisteProduto(Produtos) && VendaValidacao.TemVendedor(IdVendedor);
        }
    }
}

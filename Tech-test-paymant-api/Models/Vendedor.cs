﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Servicos;
using Tech_test_paymant_api.Validators;

namespace Tech_test_paymant_api.Models
{
    /// <summary>
    /// Classe Vendedor
    /// </summary>
    public class Vendedor
    {
        /// <summary>
        /// Id do vendedor
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Cpf do vendedor
        /// </summary>
        public String Cpf { get; set; }

        /// <summary>
        /// Nome do vendedor
        /// </summary>
        public String Nome { get; set; }

        /// <summary>
        /// Email do vendedor
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// Telefone do vendedor
        /// </summary>
        public String Telefone { get; set; }

        /// <summary>
        /// Método utilizado para validar os campos do vendedor
        /// </summary>
        /// <returns></returns>
        public bool EhValido() 
        {
            return EmailService.EhValido(Email) && CpfService.EhValido(Cpf) && 
                   VendedorValidacao.ValidarNome(Nome) && 
                   VendedorValidacao.ValidarTelefone(Telefone);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tech_test_paymant_api.Models
{
    /// <summary>
    /// Classe de Produtos
    /// </summary>
    public class Produto
    {
        /// <summary>
        /// Id do produto
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nome do produto
        /// </summary>
        public String NomeProduto { get; set; }
        /// <summary>
        /// Descrição do produto
        /// </summary>
        public String Descricao { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tech_test_paymant_api.Interfaces
{
    interface IRepositorio<T> where T : class
    {
        IEnumerable<T> Listar();
        bool Adicionar(T entity);
        bool Alterar(T entity);
        bool Remover(int id);
        T BuscarPorId(int id);
    }
}

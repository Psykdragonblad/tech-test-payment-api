﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Servicos;

namespace Tech_test_paymant_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private VendaService servico;

        public VendaService Servico
        {
            get { return servico ?? new VendaService(); }
            set { servico = value; }
        }


        /// <summary>
        /// Lista todas as vendas
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Lista obtida com sucesso</response>
        /// <response code="500">Ocorreu um erro ao trazer a lista</response>
        [HttpGet]
        [Route("Listar")]
        public IActionResult Listar()
        {
            try
            {
                return Ok(Servico.Listar());
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }            
        }

        /// <summary>
        /// Cadastra uma venda
        /// </summary>
        /// <param name="venda">Objeto a ser cadastrado</param>
        /// <returns>Ok</returns>
        /// <response code="200">Cadastro realizado com sucesso</response>
        /// <response code="500">Ocorreu um erro ao realizar o cadastro</response>
        [HttpPost]
        [Route("CadastrarVenda")]
        public IActionResult CadastrarVenda([FromBody] Venda venda) 
        {
            try
            {
                return Ok(Servico.Adicionar(venda));
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Altera uma venda específica
        /// </summary>
        /// <param name="venda">A venda que será alterada</param>
        /// <response code="200">Alteração realizada com sucesso</response>
        /// <response code="500">Ocorreu um erro ao realizar a alteração</response>
        [HttpPut]
        [Route("Alterar")]
        public IActionResult AlterarVenda([FromBody] Venda venda)
        {
            try
            {
                return Ok(Servico.Alterar(venda));
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError,e.Message);
            }
        }

        /// <summary>
        /// Exclui uma venda
        /// </summary>
        /// <param name="id">Id da venda</param>
        /// <returns></returns>
        /// <response code="200">Exclusão realizada com sucesso</response>
        /// <response code="500">Ocorreu um erro ao realizar a exclusão</response>
        [HttpDelete]
        [Route("Excluir/{id}")]
        public IActionResult ExcluirVenda(int id)
        {
            try
            {
                return Ok(Servico.Remover(id));
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Busca uma venda específica
        /// </summary>
        /// <param name="id">Id da venda</param>
        /// <returns></returns>
        /// <response code="200">Busca realizada com sucesso</response>
        /// <response code="500">Ocorreu um erro ao realizar a busca da venda</response>
        [HttpGet]
        [Route("Buscar/{id}")]
        public IActionResult BuscarVenda(int id)
        {
            try
            {
                return Ok(Servico.BuscarPorId(id));
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
            
        }
    }
}

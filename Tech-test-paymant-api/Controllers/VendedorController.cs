﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Servicos;

namespace Tech_test_paymant_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private VendedorService servico;

        public VendedorService Servico
        {
            get { return servico ?? new VendedorService(); }
            set { servico = value; }
        }

        /// <summary>
        /// Lista todos os vendedores
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Busca realizada com sucesso</response>
        /// <response code="500">Ocorreu um erro ao realizar a busca do vendedor</response>
        [HttpGet]
        [Route("Listar")]
        public IActionResult Listar()
        {
            try
            {
                return Ok(Servico.Listar());
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        /// <summary>
        /// Cadastra uma vendedor
        /// </summary>
        /// <param name="vendedor">Vendedor a ser cadastrado</param>
        /// <returns></returns>
        /// <response code="200">Vendedor cadastrado com sucesso</response>
        /// <response code="500">Ocorreu um erro ao realizar ao cadastrar o vendedor</response>
        [HttpPost]
        [Route("CadastrarVendedor")]
        public IActionResult CadastrarVendedor([FromBody] Vendedor vendedor)
        {
            try
            {
                return Ok(Servico.Adicionar(vendedor));
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tech_test_paymant_api.Validators
{
    public class VendedorValidacao
    {
        public static bool ValidarNome(string nome) 
        {
            if (String.IsNullOrEmpty(nome)) 
            {
                throw new Exception("O nome não pode ser nulo ou vazio");
            }

            if (nome.Length > 150) 
            {
                throw new Exception("O nome não pode ser maior que 150 caractéres");
            }

            return true;
        }

        public static bool ValidarTelefone(string telefone)
        {
            if (String.IsNullOrEmpty(telefone))
            {
                throw new Exception("O telefone não pode ser nulo ou vazio");
            }

            if (telefone.Length > 11)
            {
                throw new Exception("O telefone não pode ser maior que 11 caractéres");
            }
            if (!Regex.Match(telefone, @"^[0-9]+$").Success)
            {
                throw new Exception("O telefone não pode conter caractéres");
            }

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Models;

namespace Tech_test_paymant_api.Validators
{
    public class VendaValidacao
    {
        public static bool ExisteProduto(List<Produto> quantidade) 
        {
            if(quantidade == null)
                throw new Exception("Não existe produto");
            return true;
        }

        public static bool TemVendedor(int id)
        {
            if (id == 0)
                throw new Exception("Não existe vendedor");
            return true;
        }
    }
}

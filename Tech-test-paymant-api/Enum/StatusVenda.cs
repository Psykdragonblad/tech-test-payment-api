﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tech_test_paymant_api.Enum
{
    [Flags]
    public enum StatusVenda
    {
        PagamentoAprovado = 0,
        EnviadoTransportadora = 1,
        Entregue = 2,
        Cancelada = 3,
        AguardandoPagamento = 4

    }
}

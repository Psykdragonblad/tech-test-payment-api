﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Enum;
using Tech_test_paymant_api.Interfaces;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Servicos;

namespace Tech_test_paymant_api.Repositorios
{
    public class VendaRepositorio : IRepositorio<Venda>
    {
         static List<Venda> context = new List<Venda>();

        /// <summary>
        /// Adicionar uma nova venda
        /// </summary>
        /// <param name="entity">Venda a ser cadastrada</param>
        /// <returns></returns>
        public bool Adicionar(Venda entity)
        {
            if (!entity.EhValido())
                throw new Exception("Venda com problemas com o vendedor e/ou produto");

            if (!Repositorios.VendedorRepositorio.ExisteVendedor(entity.IdVendedor))
                throw new Exception("Vendedor não encontrado");

            entity.Id = AutoIncrementoService.Incrementar(context.Select(i => i.Id).ToList());
            entity.DataVenda = DateTime.Now;
            entity.StatusVenda = StatusVenda.AguardandoPagamento;
            foreach (var item in entity.Produtos)
            {
                item.Id = AutoIncrementoService.Incrementar(entity.Produtos.Select(i => i.Id).ToList());
            }
            context.Add(entity);
            return true;
        }

        /// <summary>
        /// Altera uma venda específica
        /// </summary>
        /// <param name="entity">Venda ao ser alterada</param>
        /// <returns></returns>
        public bool Alterar(Venda entity)
        {
            if(context.Count == 0 || context.Find(x => x.Id == entity.Id) == null)
                throw new Exception("Venda não encontrada");

            foreach (var item in context)
            {
                if (item.Id == entity.Id) 
                {
                    if (entity.IdVendedor == 0 || !Repositorios.VendedorRepositorio.ExisteVendedor(entity.IdVendedor))
                        throw new Exception("Vendedor não encontrado");

                    if (StatusVendaService.MudarStatus(item.StatusVenda, entity.StatusVenda))
                    {
                        item.DataAlteracao = DateTime.Now;
                        item.StatusVenda = entity.StatusVenda;
                    }

                    foreach (var produto in item.Produtos)
                    {
                        foreach (var prod in entity.Produtos)
                        {
                            if (produto.Id == prod.Id) 
                            {
                                produto.Descricao = prod.Descricao;
                                produto.NomeProduto = prod.NomeProduto;
                            }
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Traz uma lista de vendas
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Venda> Listar()
        {
            return context.ToList();
        }

        /// <summary>
        /// Remove uma venda pelo id
        /// </summary>
        /// <param name="id">Id da venda</param>
        /// <returns></returns>
        public bool Remover(int id)
        {
            var venda = context.Where(x => x.Id == id).FirstOrDefault();
            return context.Remove(venda);
        }

        /// <summary>
        /// Busca uma venda pelo id
        /// </summary>
        /// <param name="id">Id da venda</param>
        /// <returns></returns>
        public Venda BuscarPorId(int id)
        {
            return context.Find(x => x.Id == id);            
        }
    }
}

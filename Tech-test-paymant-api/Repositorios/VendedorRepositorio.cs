﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Interfaces;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Servicos;

namespace Tech_test_paymant_api.Repositorios
{
    public class VendedorRepositorio : IRepositorio<Vendedor>
    {
        static List<Vendedor> context = new List<Vendedor>();

        /// <summary>
        /// Retorna se existe um vendedor ou não (Utilizado para simular um banco)
        /// </summary>
        /// <param name="id">Id do vendedor</param>
        /// <returns></returns>
        public static bool ExisteVendedor(int id) 
        {
            return context.Exists(x => x.Id == id);
        }

        /// <summary>
        /// Adiciona um novo vendedor
        /// </summary>
        /// <param name="entity">Vendedor a ser cadastrado</param>
        /// <returns></returns>
        public bool Adicionar(Vendedor entity)
        {
            if (entity.EhValido())
            {
                entity.Id = AutoIncrementoService.Incrementar(context.Select(i => i.Id).ToList());
                context.Add(entity);                
            }
            return true;
        }

        /// <summary>
        /// Altera um vendedor
        /// </summary>
        /// <param name="entity">Vendedor a ser alterado</param>
        /// <returns></returns>
        public bool Alterar(Vendedor entity)
        {
            if (context.Count == 0 || context.Find(x => x.Id == entity.Id) == null)
                throw new Exception("Vendedor não encontrado");

            foreach (var item in context)
            {
                if (item.Id == entity.Id && item.EhValido())
                {
                    item.Cpf = entity.Cpf;
                    item.Email = entity.Email;
                }
            }
            return true;
        }

        /// <summary>
        /// Busca um vendedor pelo id
        /// </summary>
        /// <param name="id">Id do vendedor</param>
        /// <returns></returns>
        public Vendedor BuscarPorId(int id)
        {
            return context.Find(x => x.Id == id);
        }

        /// <summary>
        /// Traz uma lista de vendedores
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vendedor> Listar()
        {
            return context.ToList();
        }

        /// <summary>
        /// Remove um vendedor
        /// </summary>
        /// <param name="id">Id do vendedor que será removido</param>
        /// <returns></returns>
        public bool Remover(int id)
        {
            var venda = context.Where(x => x.Id == id).FirstOrDefault();
            return context.Remove(venda);
        }
    }
}

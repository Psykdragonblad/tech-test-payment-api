﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Enum;

namespace Tech_test_paymant_api.Servicos
{
    public static class StatusVendaService
    {
        public static bool MudarStatus(StatusVenda atual, StatusVenda novo) 
        {
            switch (atual)
            {
                case StatusVenda.AguardandoPagamento: 
                    {
                        if (novo == StatusVenda.PagamentoAprovado || novo == StatusVenda.Cancelada || novo == StatusVenda.AguardandoPagamento)
                            return true;
                        break;
                    }
                case StatusVenda.PagamentoAprovado:
                    {
                        if (novo == StatusVenda.EnviadoTransportadora || novo == StatusVenda.Cancelada || novo == StatusVenda.PagamentoAprovado)
                            return true;                        
                        break;
                    }
                case StatusVenda.EnviadoTransportadora:
                    {
                        if (novo == StatusVenda.Entregue || novo == StatusVenda.EnviadoTransportadora)
                            return true;
                        break;
                    }
                case StatusVenda.Cancelada:
                    {
                        if (novo == StatusVenda.Cancelada)
                            return true;
                        break;
                    }
                default: break;
            }
            throw new Exception("Mudança de Status indevida");
        }
    }
}

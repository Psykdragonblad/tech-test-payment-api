﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Repositorios;

namespace Tech_test_paymant_api.Servicos
{
    public class VendaService
    {
        VendaRepositorio Repositorio = new VendaRepositorio();
        public IEnumerable<Venda> Listar() 
        {
            return Repositorio.Listar();
        }

        public bool Adicionar(Venda venda)
        {
            return Repositorio.Adicionar(venda);
        }

        public bool Alterar(Venda venda)
        {
            return Repositorio.Alterar(venda);
        }

        public bool Remover(int id)
        {
           return Repositorio.Remover(id);
        }

        public Venda BuscarPorId(int id)
        {
            return Repositorio.BuscarPorId(id);
        }
    }
}

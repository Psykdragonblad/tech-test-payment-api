﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Repositorios;

namespace Tech_test_paymant_api.Servicos
{
    public class VendedorService
    {
        VendedorRepositorio Repositorio = new VendedorRepositorio();
        public bool Adicionar(Vendedor vendedor)
        {
            return Repositorio.Adicionar(vendedor);
        }
        public IEnumerable<Vendedor> Listar()
        {
            return Repositorio.Listar();
        }
    }
}

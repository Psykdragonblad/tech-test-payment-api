﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tech_test_paymant_api.Servicos
{
    public class AutoIncrementoService
    {
        /// <summary>
        /// Utilizado para gerar uma sequence
        /// </summary>
        /// <param name="atual">Lista de inteiros para saber o maior valor para incrementar</param>
        /// <returns></returns>
        public static int Incrementar(List<int> atual) 
        {
            if (atual == null)
                throw new Exception("A lista não pode ser nula");

            if (atual.Count == 0)
                return 1;
            
            return atual.Max()+1;
        }
    }
}

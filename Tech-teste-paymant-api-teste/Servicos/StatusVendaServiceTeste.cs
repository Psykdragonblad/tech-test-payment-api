﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Tech_test_paymant_api.Enum;
using Tech_test_paymant_api.Servicos;

namespace Tech_teste_paymant_api_teste.Servicos
{
    [TestClass]
    public class StatusVendaServiceTeste
    {
        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode ter uma mudança para uma situação indevida")]
        public void Ao_Mudar_Status_Para_Situacao_Indevida_Deve_Ocasionar_Erro()
        {
            Assert.IsNotNull(StatusVendaService.MudarStatus(StatusVenda.PagamentoAprovado,StatusVenda.AguardandoPagamento));
        }

        [TestMethod]
        public void Ao_Mudar_Status_Corre_Trazer_True()
        {
            Assert.IsTrue(StatusVendaService.MudarStatus(StatusVenda.PagamentoAprovado, StatusVenda.Cancelada));
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Tech_test_paymant_api.Servicos;

namespace Tech_teste_paymant_api_teste.Servicos
{
    [TestClass]
    public class AutoIncrementoServiceTeste
    {
        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode ser uma lista nula")]
        public void Ao_Incrementar_A_Lista_Não_Pode_Ser_Nula()
        {
            Assert.IsNotNull(AutoIncrementoService.Incrementar(null));
        }

        [TestMethod]
        public void Ao_Incrementar_Deve_Adicionar_Mais_Um()
        {
            Assert.AreEqual(3,AutoIncrementoService.Incrementar(new List<int>(){1,2 }));
        }
    }
}

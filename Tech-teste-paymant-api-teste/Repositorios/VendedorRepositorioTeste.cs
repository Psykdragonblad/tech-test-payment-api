﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Tech_test_paymant_api.Enum;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Repositorios;

namespace Tech_teste_paymant_api_teste.Repositorios
{
    [TestClass]
    public class VendedorRepositorioTeste
    {
        VendedorRepositorio vendedorRepositorio = new VendedorRepositorio();
        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_true()
        {
            Assert.AreEqual(true, vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" }));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode cadastrar um cpf invalido")]
        public void Ao_Adicionar_Cpf_Invalido_Deve_Trazer_Excecao()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "4240047200" });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode cadastrar um email invalido")]
        public void Ao_Adicionar_Email_Invalido_Deve_Trazer_Excecao()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "testeemail.coom", Telefone = "71984389494", Cpf = "04240047200" });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode cadastrar letra no telefone")]
        public void Ao_Adicionar_Letra_No_Telefone_Deve_Trazer_Excecao()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984p89494", Cpf = "04240047200" });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode cadastrar um nome vazio")]
        public void Ao_Adicionar_Nome_Vazio_Deve_Trazer_Excecao()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "", Email = "teste@email.coom", Telefone = "7198489494", Cpf = "04240047200" });
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Tech_test_paymant_api.Enum;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Repositorios;

namespace Tech_teste_paymant_api_teste.Repositorios
{
    [TestClass]
   public class VendaRepositorioTeste
    {
        VendaRepositorio vendaRepositorio = new VendaRepositorio();
        VendedorRepositorio vendedorRepositorio = new VendedorRepositorio();

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_true()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            List<Produto> produto = new List<Produto>();
            produto.Add(new Produto() { NomeProduto = "Caneta" });
            Assert.AreEqual(true, vendaRepositorio.Adicionar(new Venda() { StatusVenda = StatusVenda.Cancelada, IdVendedor = 1, Produtos = produto }));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode cadastrar sem produto")]
        public void Ao_Adicionar_Sem_Produto_Deve_Trazer_Excecao()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            Assert.AreEqual(true, vendaRepositorio.Adicionar(new Venda() { StatusVenda = StatusVenda.AguardandoPagamento, IdVendedor = 1}));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), "Não pode cadastrar sem vendedor")]
        public void Ao_Adicionar_Sem_Vendedor_Deve_Trazer_Excecao()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            Assert.AreEqual(true, vendaRepositorio.Adicionar(new Venda() { StatusVenda = StatusVenda.AguardandoPagamento }));
        }       
    }
}

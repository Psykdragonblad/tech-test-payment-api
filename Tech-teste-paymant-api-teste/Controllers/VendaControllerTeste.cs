﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tech_test_paymant_api.Controllers;
using Tech_test_paymant_api.Enum;
using Tech_test_paymant_api.Models;
using Tech_test_paymant_api.Repositorios;

namespace Tech_teste_paymant_api_teste.Controllers
{
    [TestClass]
    public class VendaControllerTeste
    {
        VendaController venda = new VendaController();
        VendedorRepositorio vendedorRepositorio = new VendedorRepositorio();

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_200()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            List<Produto> produto = new List<Produto>();
            produto.Add(new Produto() { NomeProduto = "Caneta" });
            var resultado = venda.CadastrarVenda(new Venda { Produtos = produto, IdVendedor =1 }) as OkObjectResult;
            Assert.AreEqual(200,resultado.StatusCode);
        }

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_True()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            List<Produto> produto = new List<Produto>();
            produto.Add(new Produto() { NomeProduto = "Caneta" });
            var resultado = venda.CadastrarVenda(new Venda { Produtos = produto, IdVendedor = 1 }) as OkObjectResult;
            Assert.AreEqual(true, resultado.Value);
        }

        [TestMethod]
        public void Ao_Adicionar_Deve_Cadastrar_Status_Aguardando_Pagamento ()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            List<Produto> produto = new List<Produto>();
            produto.Add(new Produto() { NomeProduto = "Caneta" });
            var resultado = venda.CadastrarVenda(new Venda { Produtos = produto, IdVendedor=1 }) as OkObjectResult;
            var resultadoInsert = venda.BuscarVenda(1) as OkObjectResult;
            Venda venda2 = (Venda)resultadoInsert.Value;
            Assert.AreEqual(StatusVenda.AguardandoPagamento,venda2.StatusVenda);
        }
        
        [TestMethod]
        public void Ao_Alterar_Deve_Trazer_True()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            List<Produto> produto = new List<Produto>();
            produto.Add(new Produto() { NomeProduto = "Caneta" });
            var resultado = venda.CadastrarVenda(new Venda { Produtos = produto, IdVendedor = 1 }) as OkObjectResult;
                       
            produto.Add(new Produto() { NomeProduto = "Caneta2" });
            var resultadoAlteracao = venda.AlterarVenda(new Venda { Produtos = produto, IdVendedor = 1, Id = 1 }) as OkObjectResult;
            Assert.AreEqual(true, resultadoAlteracao.Value);
        }

        [TestMethod]
        public void Ao_Alterar_Deve_Trazer_Valor_Alterado_Igual()
        {
            vendedorRepositorio.Adicionar(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "04240047200" });
            List<Produto> produto = new List<Produto>();
            produto.Add(new Produto() { NomeProduto = "Caneta" });
            var resultado = venda.CadastrarVenda(new Venda { Produtos = produto, IdVendedor = 1 }) as OkObjectResult;

            produto.Add(new Produto() { NomeProduto = "Caneta2" });
            var resultadoAlteracao = venda.AlterarVenda(new Venda { Produtos = produto }) as OkObjectResult;
            var resultadoBusca = venda.BuscarVenda(1) as OkObjectResult;
            Venda venda2 = (Venda)resultadoBusca.Value;
            Assert.IsNotNull(venda2.Produtos.Exists(x => x.NomeProduto == "Caneta1"));
        }
    }
}

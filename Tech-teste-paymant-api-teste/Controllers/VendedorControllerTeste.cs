﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Tech_test_paymant_api.Controllers;
using Tech_test_paymant_api.Models;

namespace Tech_teste_paymant_api_teste.Controllers
{
    [TestClass]
   public class VendedorControllerTeste
    {
        VendedorController vendedor = new VendedorController();

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_200()
        {            
            var resultado = vendedor.CadastrarVendedor(new Vendedor() {Nome="João",Email="teste@email.coom",Telefone="71984389494",Cpf= "04240047200" }) as OkObjectResult;
            Assert.AreEqual(200, resultado.StatusCode);
        }

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_500_Cpf()
        {
            var resultado = vendedor.CadastrarVendedor(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "71984389494", Cpf = "111" }) as ObjectResult;
            Assert.AreEqual(500, resultado.StatusCode);
        }

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_500_Email()
        {
            var resultado = vendedor.CadastrarVendedor(new Vendedor() { Nome = "João", Email = "testeemail.coom", Telefone = "71984389494", Cpf = "04240047200" }) as ObjectResult;
            Assert.AreEqual(500, resultado.StatusCode);
        }

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_500_Telefone()
        {
            var resultado = vendedor.CadastrarVendedor(new Vendedor() { Nome = "João", Email = "teste@email.coom", Telefone = "7oo4389494", Cpf = "04240047200" }) as ObjectResult;
            Assert.AreEqual(500, resultado.StatusCode);
        }

        [TestMethod]
        public void Ao_Adicionar_Deve_Trazer_Vendedor()
        {
            var resultado = vendedor.CadastrarVendedor(new Vendedor() { Nome = "João", Email = "test@email.coom", Telefone = "74389494", Cpf = "04240047200" }) as ObjectResult;
            var resultadoBusca = vendedor.Listar() as ObjectResult;
            List<Vendedor> vendedorAux = (List<Vendedor>)resultadoBusca.Value;
            Assert.IsTrue(vendedorAux.Count>0);
        }
    }
}
